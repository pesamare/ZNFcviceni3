<?php

namespace App\Model;

use App\Model\PidModel;

class UtilityModel extends BaseModel
{
    /** @var PidModel - model pro management rc*/
    private $pidModel;

    public function injectDependencies(PidModel $pidModel)
    {
        $this->pidModel = $pidModel;
    }
    /**
     * Metoda detekuje pohlaví -1 = nedefinováno, 0 - žena, 1 - muž
     * @param int  $id rodného čísla
     */
    public function isMan($id)
    {
        if(!$id) return -1;
        $pid = $this->pidModel->getPid($id);
        if(!$pid) return -1;
        $rc = substr($pid['name'],2,2);
        return $rc<50;
    }

    /**
     * Metoda detekuje datum narození
     * @param int  $id rodného čísla
     */
    public function getBirthDay($id)
    {
        if($id=="" || $id <= 99999999)return "nedef";
        if($id<1000000000 && $id> 99999999){              //9 mistna
            $year=(floor($id/10000000));
            if($year>53){
                $year=1800+$year;
            }else{
                $year=1900+$year;
            }
            $month=($id/100000)%100;
            if($month>50)$month-=50;
            $day=($id/1000)%100;
            return $day . "." . $month . ". " .$year;
        }else{                                                       //10 mistna
            $year=(floor($id/100000000));
            if($year>53){
                $year=1900+$year;
            }else{
                $year=2000+$year;
            }
            $month=($id/1000000)%100;
            if($month>50)$month-=50;
            $day=($id/10000)%100;
            return $day . "." . $month . ". " .$year;
        }
    }

    /**
     * Metoda detekuje pohlavi
     * @param int  $id rodného čísla
     */
    public function getSex($id)
    {
        if($id=="" || $id <= 99999999)return "nedef";
        if($id<1000000000){              //9 mistna
            $month=($id/100000)%100;
            if($month>50)return "zena";
            return "muz";

        }else{                                //10 mistna
            $month=($id/1000000)%100;
            if($month>50)return "zena";
            return "muz";
        }
    }

}