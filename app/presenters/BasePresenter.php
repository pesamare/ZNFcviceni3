<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;
use \App\Model\UtilityModel;

/**
 * Class BasePresenter
 * @package App\Presenters
 */
abstract class BasePresenter extends Presenter
{


    private $utilityModel;

    public function __Construct(
        UtilityModel $utilityModel
    )
    {
        $this->utilityModel = $utilityModel;
    }

    protected function beforeRender(){
        $this->template->addFilter('phone', function ($number) {
            $string="!!" . $number;
            if($number!="" || $number>999999999)
                $string="+420 " . ($number/1000000)%1000 ." " . ($number/1000)%1000 . " " . $number%1000;
            return $string;
        });

        $this->template->addFilter('sex', function ($number) {
            return $this->utilityModel->getSex($number);

        });

        $this->template->addFilter('birthday', function ($number) {

            return $this->utilityModel->getBirthday($number);
        });

}

}


